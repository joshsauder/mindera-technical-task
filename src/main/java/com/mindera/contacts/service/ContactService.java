package com.mindera.contacts.service;

import com.mindera.contacts.exception.InvalidResourceException;
import com.mindera.contacts.exception.ResourceNotFoundException;
import com.mindera.contacts.model.Contact;
import com.mindera.contacts.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.util.ReflectionUtils;

import javax.validation.ConstraintViolationException;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

@Service
public class ContactService {

    private final String RESOURCE = "Contact";
    private final String ID_FIELD = "id";

    private ContactRepository contactRepository;

    @Autowired
    public ContactService(ContactRepository repository){
        this.contactRepository = repository;
    }

    public Contact save(Contact contact){
        try {
            return contactRepository.save(contact);
        } catch(ConstraintViolationException e){
            throw new InvalidResourceException(RESOURCE);
        } catch(TransactionSystemException e){
            throw new InvalidResourceException(RESOURCE);
        }
    }

    public Contact getContactById(Long id){
        //if not exist, throw new ResourceNotFoundException
        return contactRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(RESOURCE, ID_FIELD, id.toString()));
    }

    public List<Contact> getContacts(){
        return contactRepository.getAllContacts();
    }

    public void deleteContact(Long id){
        try {
            contactRepository.deleteById(id);
        }catch(EmptyResultDataAccessException e){
            throw new ResourceNotFoundException(RESOURCE, ID_FIELD, id.toString());
        }
    }

    public Contact putContact(Long id, Contact contact){
        Contact rec = getContactById(id);

        //update fields within existing contact
        rec.setLastName(contact.getLastName());
        rec.setFirstName(contact.getFirstName());
        rec.setGender(contact.getGender());
        rec.setAge(contact.getAge());

        return save(rec);
    }

    public Contact patchContact(Long id, Map<String, Object> patchedData){
        //Patch data invalid if null, empty, or attempting to update the id field
        if(patchedData == null || patchedData.isEmpty() || 
        (patchedData.get(ID_FIELD) != null && !patchedData.get(ID_FIELD).equals(id))){
            throw new InvalidResourceException(RESOURCE);
        }

        Contact contact = getContactById(id);

        //map patch data to existing Contact
        patchedData.forEach((k, v) -> {
            Field field = ReflectionUtils.findField(Contact.class, k);
            field.setAccessible(true);
            //field, target, value
            ReflectionUtils.setField(field, contact, v);
        });

        return save(contact);
    }
}
