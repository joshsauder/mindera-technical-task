package com.mindera.contacts.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidResourceException extends RuntimeException {
    private String resourceName;

    public InvalidResourceException (String resourceName) {
        super(String.format("%s resource is not valid", resourceName));

        this.resourceName = resourceName;
    }

    public String getResourceName() {
        return resourceName;
    }
}
