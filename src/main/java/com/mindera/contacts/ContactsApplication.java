package com.mindera.contacts;

import com.auth0.jwt.JWT;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static com.mindera.contacts.config.SecurityConstants.*;

@SpringBootApplication
public class ContactsApplication {

	public static void main(String[] args) {

		SpringApplication.run(ContactsApplication.class, args);
	}

	@Bean
	CommandLineRunner createKey(){
		return args -> {
			String key = JWT.create()
			.withSubject("Josh Sauder")
			.withAudience(AUD)
			.withIssuer(ISS)
			.withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
			.sign(HMAC512(JWT_SECRET.getBytes()));
			
			System.out.println("--- START JWT TOKEN ---");
			System.out.println(key);
			System.out.println("--- END JWT TOKEN ---");
		};
	}

}
