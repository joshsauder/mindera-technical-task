package com.mindera.contacts.repository;

import com.mindera.contacts.model.Contact;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ContactRepository extends JpaRepository<Contact, Long> {

    @Query(value = "SELECT * FROM contacts", nativeQuery = true)
    List<Contact> getAllContacts();

    @Query(value = "SELECT * FROM contacts WHERE id = ?1", nativeQuery = true)
    Contact getContactById(Long id);

}
