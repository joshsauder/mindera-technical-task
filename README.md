# Contacts DB

A simple web service to store contact information

## Prerequisites

To get started, you'll need to have the following dependencies:

```txt
Java 8 (or higher)
Docker
Postman (optional)
```

### Building The App

To build the application, you first need to build a new package using Maven:

```bash
./mvnw clean package
```

You will then need to build the Docker image using the following command:

```bash
docker build ./ -t mindera-technical-task
```

If you do not have the PostgresDB Docker Image, please run the following:

```bash
docker pull postgres
```


### Running The Application

Once you have built the Docker image, you will then need to launch the application.

```bash
docker-compose up
```

This will deploy the app at http://localhost:8080. Since there isn't a way to validate users, a JWT token will be listed in the console. This key will allow you to test the API using Postman or a similar API client. To use this key, you will need to set the Authorization header to "Bearer <TOKEN>."

The Swagger UI can be viewed at: http://localhost:8080/swagger-ui.html

You can stop all containers started by ```docker-compose up```, by using the following command:

```bash
docker-compose down
```

### Running the Tests

To run the integration tests, you will need to run the PostgresDB Docker Image. 

```bash
docker run -it -e "POSTGRES_PASSWORD=SIoNiEstLEST" -e "POSTGRES_USER=testuser" -e "POSTGRES_DB=backend_test"  -p 5412:5432 postgres:latest
```