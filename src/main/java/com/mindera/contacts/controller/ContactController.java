package com.mindera.contacts.controller;

import com.mindera.contacts.model.Contact;
import com.mindera.contacts.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/contacts")
public class ContactController {

    private ContactService contactService;

    @Autowired
    public ContactController(ContactService service){
        this.contactService = service;
    }


    @GetMapping
    public ResponseEntity<List<Contact>> getContacts(@AuthenticationPrincipal String user){
        List<Contact> contacts = contactService.getContacts();
        return ResponseEntity.ok().body(contacts);
    }

    @GetMapping("/{id}")
    public Contact getContactById(@PathVariable("id") Long id, @AuthenticationPrincipal String user){
        return contactService.getContactById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteContact(@PathVariable("id") Long id, @AuthenticationPrincipal String user){
            contactService.deleteContact(id);
    }

    @PutMapping("/{id}")
    public Contact putContact(@PathVariable("id") Long id, @RequestBody Contact contact, @AuthenticationPrincipal String user){
        return contactService.putContact(id, contact);
    }

    @PostMapping
    public Contact postContact(@RequestBody Contact contact, @AuthenticationPrincipal String user){
        return contactService.save(contact);
    }

    @PatchMapping("/{id}")
    public Contact patchContact(@PathVariable("id") Long id, @RequestBody Map<String, Object> patchData, @AuthenticationPrincipal String user){
        return contactService.patchContact(id, patchData);
    }

}
