package com.mindera.contacts.config;

public class SecurityConstants {
    public static final String HEADER_STRING = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String AUD = "mindera-aud";
    public static final String ISS = "mindera-api";
    public static final String JWT_SECRET = "z%C*F-JaNdRfUjXn2r5u8x/A?D(G+KbPeShVkYp3s6v9y$B&E)H@McQfTjWnZq4t";
    //1 DAY
    public static final long EXPIRATION_TIME = 86_400_000;
}
