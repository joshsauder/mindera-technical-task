package com.mindera.contacts;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mindera.contacts.exception.InvalidResourceException;
import com.mindera.contacts.model.Contact;
import com.mindera.contacts.repository.ContactRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations="classpath:application-test.properties")
public class ContactsIntegrationTests {

    private final String CONTACT_URL = "/api/contacts";

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ContactRepository repository;

    /**
     *
     * Test Put Contact with valid Contact
     * Should return 200 with updated Contact
     */
    @Test
    @WithMockUser()
    public void testPutContact() throws Exception{
        Contact oldContact = buildNewContact("test1", "test1", 4, "M");
        Contact newContact = buildNewContact("test2", "test2", 4, "M");

        Mockito.doReturn(Optional.of(oldContact)).when(repository).findById(any(Long.class));
        Mockito.doReturn(newContact).when(repository).save(any(Contact.class));

        mvc.perform(put(CONTACT_URL + "/4")
                .content(mapper.writeValueAsString(newContact))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(newContact)));

    }

    /**
     *
     * Test Put Contact with invalid contact
     * Should return 400
     */
    @Test
    @WithMockUser()
    public void testPutInvalidContact() throws Exception{
        Contact oldContact = buildNewContact("test1", "test1", 4, "M");
        Contact newContact = buildNewContact("test2", null, 4, "M");

        Mockito.doReturn(Optional.of(oldContact)).when(repository).findById(any(Long.class));
        Mockito.doThrow(InvalidResourceException.class).when(repository).save(any(Contact.class));

        mvc.perform(put(CONTACT_URL + "/4")
                .content(mapper.writeValueAsString(newContact))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    /**
     *
     * Test Patch Contact with valid patch data
     * Should return 200 with updated contact
     */
    @Test
    @WithMockUser()
    public void testPatchContact() throws Exception{
        Contact oldContact = buildNewContact("test1", "test1", 4, "M");
        Contact newContact = buildNewContact("test2", "test3", 4, "M");

        Map<String, Object> patch = new HashMap<String, Object>() {{
            put("firstName", "test2");
            put("lastName", "test3");
        }};

        Mockito.doReturn(Optional.of(oldContact)).when(repository).findById(any(Long.class));
        Mockito.doReturn(newContact).when(repository).save(any(Contact.class));

        mvc.perform(patch(CONTACT_URL + "/4")
                .content(mapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(newContact)));

    }

    /**
     *
     * Test Patch Contact and update last name to invalid value
     * Should return 400
     */
    @Test
    @WithMockUser()
    public void testPatchInvalidContact() throws Exception{
        Contact oldContact = buildNewContact("test1", "test1", 4, "M");

        Map<String, Object> patch = new HashMap<String, Object>() {{
            put("lastName", null);
        }};

        Mockito.doReturn(Optional.of(oldContact)).when(repository).findById(any(Long.class));
        Mockito.doThrow(InvalidResourceException.class).when(repository).save(any(Contact.class));

        mvc.perform(patch(CONTACT_URL + "/4")
                .content(mapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    /**
     *
     * Test Patch Contact and update ID
     * Should return 400
     */
    @Test
    @WithMockUser()
    public void testPatchUpdateID() throws Exception{
        Contact oldContact = buildNewContact("test1", "test1", 4, "M");

        Map<String, Object> patch = new HashMap<String, Object>() {{
            put("id", "10");
        }};

        Mockito.doReturn(Optional.of(oldContact)).when(repository).findById(any(Long.class));

        //pass in ID = 4.
        mvc.perform(patch(CONTACT_URL + "/4")
                .content(mapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    private Contact buildNewContact(String first, String last, int age, String gender){
        Contact contact = new Contact();
        contact.setAge(age);
        contact.setFirstName(first);
        contact.setLastName(last);
        contact.setGender(gender);

        return contact;
    }
}