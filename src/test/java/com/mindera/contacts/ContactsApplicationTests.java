package com.mindera.contacts;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mindera.contacts.controller.ContactController;
import com.mindera.contacts.exception.InvalidResourceException;
import com.mindera.contacts.exception.ResourceNotFoundException;
import com.mindera.contacts.model.Contact;
import com.mindera.contacts.service.ContactService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.validation.ConstraintViolationException;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ContactController.class)
class ContactsApplicationTests {

	private final String CONTACT_URL = "/api/contacts";

	ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private MockMvc mvc;

	@MockBean
	private ContactService service;


	/**
	 * Add a new Contact
	 * Should return 200 and the contact info
	 */
	@Test
	@WithMockUser()
	public void addContact() throws Exception{
		Contact contact = buildNewContact("test", "test", 4, "M");

		Mockito.doReturn(contact).when(service).save(any(Contact.class));

		mvc.perform(post(CONTACT_URL)
				.content(mapper.writeValueAsString(contact))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().json(mapper.writeValueAsString(contact)));

	}

	/**
	 * Add a contact without body
	 * Should return 400
	 */
	@Test
	@WithMockUser()
	public void addEmptyContact() throws Exception {
		Mockito.doThrow(InvalidResourceException.class).when(service).save(any(Contact.class));

		mvc.perform(post(CONTACT_URL)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	/**
	 * Add a contact without required fields
	 * Should return 400
	 */
	@Test
	@WithMockUser()
	public void addInvalidContact() throws Exception {
		Contact contact = buildNewContact("test", null, 4, "M");

		Mockito.doThrow(InvalidResourceException.class).when(service).save(any(Contact.class));

		mvc.perform(post(CONTACT_URL)
				.content(mapper.writeValueAsString(contact))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	/**
	 * Add a contact without Auth
	 * Should return 403
	 */
	@Test
	public void addContactWithoutKey() throws Exception {
		Contact contact = buildNewContact("test", "test", 4, "M");

		Mockito.doThrow(ConstraintViolationException.class).when(service).save(any(Contact.class));

		mvc.perform(post(CONTACT_URL)
				.content(mapper.writeValueAsString(contact))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isForbidden());
	}

	/**
	 * Add a contact using incorrect Post route
	 * Should return 404
	 */
	@Test
	@WithMockUser()
	public void testInvalidPostRoute() throws Exception {
		Contact contact = buildNewContact("test", "test", 4, "M");

		mvc.perform(post("/api/contact")
				.content(mapper.writeValueAsString(contact))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	/**
	 * Find a contact using existing ID
	 * Should return 200 with the contact info
	 */
	@Test
	@WithMockUser()
	public void testGetContactById() throws  Exception {
		Contact contact = buildNewContact("test", "test", 4, "M");

		Mockito.doReturn(contact).when(service).getContactById(any(Long.class));

		mvc.perform(get(CONTACT_URL + "/4"))
				.andExpect(status().isOk())
				.andExpect(content().json(mapper.writeValueAsString(contact)));
	}

	/**
	 * Find contact who does not exist
	 * Should return 404
	 */
	@Test
	@WithMockUser()
	public void testGetContactNotExist() throws  Exception {
		Mockito.doThrow(ResourceNotFoundException.class).when(service).getContactById(any(Long.class));

		mvc.perform(get(CONTACT_URL + "/4"))
				.andExpect(status().isNotFound());
	}

	/**
	 * Find all contacts
	 * Should return 200, with all existing contacts
	 */
	@Test
	@WithMockUser()
	public void testGetContacts() throws Exception {
		List<Contact> contactList = Arrays.asList(
				buildNewContact("test1", "test1", 4, "M"),
				buildNewContact("test2", "test2", 10, "F"),
				buildNewContact("test3", "test3", 16, "F")
		);

		Mockito.doReturn(contactList).when(service).getContacts();

		mvc.perform(get(CONTACT_URL))
				.andExpect(status().isOk())
				.andExpect(content().json(mapper.writeValueAsString(contactList)));
	}

	/**
	 * Test Put contacts with valid contact
	 * Should return 200, with updated contact
	 */
	@Test
	@WithMockUser()
	public void testPutContact() throws Exception {
		Contact newContact = buildNewContact("test2", "test2", 4, "M");

		Mockito.doReturn(newContact).when(service).putContact(any(Long.class), any(Contact.class));

		mvc.perform(put(CONTACT_URL + "/4")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(newContact)))
				.andExpect(status().isOk())
				.andExpect(content().json(mapper.writeValueAsString(newContact)));
	}

	/**
	 * Put contacts with invalid new Contact
	 * Should return 400
	 */
	@Test
	@WithMockUser()
	public void testPutWithInvalidContact() throws Exception {
		Contact newContact = buildNewContact("test2", null, 4, "M");

		Mockito.doThrow(InvalidResourceException.class).when(service).putContact(any(Long.class), any(Contact.class));

		mvc.perform(put(CONTACT_URL + "/4")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(newContact)))
				.andExpect(status().isBadRequest());
	}
	
	/**
	 * Patch contacts with valid patch data
	 * Should return 200, with updated contact
	 */
	@Test
	@WithMockUser()
	public void testPatchWithValidPatchData() throws Exception {
		Contact patchContact = buildNewContact("test2", "test", 4, "M");

		Map<String, Object> patch = new HashMap<String, Object>() {{
			put("lastName", "test");
		}};

		Mockito.doReturn(patchContact).when(service).patchContact(any(Long.class), any(Map.class));

		mvc.perform(patch(CONTACT_URL + "/4")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(patch)))
				.andExpect(status().isOk())
				.andExpect(content().json(mapper.writeValueAsString(patchContact)));;
	}


	/**
	 * Patch contacts with invalid Contact data
	 * Should return 400
	 */
	@Test
	@WithMockUser()
	public void testPatchWithInvalidPatchData() throws Exception {
		Map<String, Object> patch = new HashMap<String, Object>() {{
			put("id", 3);
		}};

		Mockito.doThrow(InvalidResourceException.class).when(service).patchContact(any(Long.class), any(Map.class));

		mvc.perform(patch(CONTACT_URL + "/4")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(patch)))
				.andExpect(status().isBadRequest());
	}


	private Contact buildNewContact(String first, String last, int age, String gender){
		Contact contact = new Contact();
		contact.setAge(age);
		contact.setFirstName(first);
		contact.setLastName(last);
		contact.setGender(gender);

		return contact;
	}

}
